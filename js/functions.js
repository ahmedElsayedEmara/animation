jQuery(function ($) {
    "use strict";
  
    $(".study-block ").click(function(){
        $(".project-studies").addClass("selecteds");
        $(".blocks").css({
            position:"absolute",
            display: "block", 
          
        },200).animate({ 
            right: 0, 
            left: 0
        });
        $(this).addClass("emara").siblings().removeClass("emara").hide();
    });

    $(".closes").click(function(){
        $(".project-studies").removeClass("selecteds");
        $(".study-block").removeClass("emara").fadeTo( 'slow', 1);
        $(".blocks").css({ 
            display: "none", 
            position: "relative",
          
          
        },100).animate({ 
            right: " -500px", 
            left : "3000px"
        });
    });
   
});